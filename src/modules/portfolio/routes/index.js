export default [
  {
    path: "/portfolio_new",
    name: "portfolio_new",
    component: () => import(/* webpackChunkName: "Index" */ '../pages/Index'),
    title: "New Portfolio",
    meta: { requiresAuth: true, layout: 'profile-layout' },
  }
];