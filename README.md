# README #

Este respositorio pertenece al nuevo diseño de Portfolio

### Branchs activas

Listado de las ramas activas para el desarrollo de nuevo portfolio

- sesocio-portfolio-module:
  - master
  - SSSPA-17: contiene las modificaciones de la tabla de cryptos. Ver tarea en [https://blockchain.atlassian.net/browse/SSSPA-17]

- ui-components:
  - ALOCA-1519

- sesocio-pwa
  - ALOCA-1519: rama actual donde se realizan los cambios que necesita el nuevo portfolio
  - new_layout: rama donde se deploya el nuevo portfolio (para no interferir con staging)

- sesocio-cvu-module:
  - ALOCA-1440: rama donde se modifico el endopint a la versión 3 de la api
