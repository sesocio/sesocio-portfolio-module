import axios from "axios";
import camelcaseKeys from "camelcase-keys";

// Obtiene un color random
const randomColor = () =>  {
  return "#" + Math.floor(Math.random()*0xFFFFFF).toString(16).padStart(6, '0').toUpperCase();
};

const getValuedBalance = async ({ commit, dispatch }, payload) => {
  commit("SET_LOADING", true);
  await commit("CLEAR_DATA");
  const type = payload || "crypto";
  try {
    const url =
      process.env.VUE_APP_API_BASE_URL +
      "/api/v3" +
      `/portfolios/total_amounts?type=${type}`;

    // Datos del saldo valorizado con v3
    await axios.get(url)
      .then(async (resp) => {
        commit("SET_VALUED_BALANCE", camelcaseKeys(resp.data));
        // Solo para crypto y stablecoins
        if (type == "crypto" || type == "stable_coin") {
          await dispatch("getChartData", type);
        }
        commit("SET_LOADING", false);
      })
      .catch((e) => { console.log(e) });

  } catch (err) {
    return err;
  }
}

// Obtiene el listado de monedas del usuario
const getUserCurrencies = async ({ commit }) => {
  try {
    const url =
      process.env.VUE_APP_API_BASE_URL +
      "/api/v3" +
      "/users/get_data";

    await axios.get(url)
      .then(resp => {
        // Cambia USD por IUSD
        const currenciesArray = resp.data.currencies.map(
          currency => currency == "USD" ? "IUSD" : currency
        );
        commit("SET_USER_CURRENCIES", currenciesArray);
        commit("SET_SELECTED_CURRENCY", currenciesArray[0]);
      })
      .catch((e) => { console.log(e) });

  } catch (err) {
    console.error(err);
  }
}

// Obtiene los datos de los gráficos según moneda y tipo
const getChartData = async ({ commit, state }, payload) => {
  const type = payload || "crypto";
  try {
    const url =
      process.env.VUE_APP_API_BASE_URL +
      "/api/v3" +
      `/portfolios/get_chart_data?currency=${state.selectedCurrency}&type=${type}`;

    // Datos del saldo valorizado con v3
    await axios.get(url)
      .then((resp) => {
        const chartData = resp.data.map(item => ({
          ...item,
          // Si viene sin color le asigno uno random
          color: item.color ? item.color : randomColor(),
        }));

        commit("SET_CHART_DATA", chartData);
      })
      .catch((e) => { console.log(e) });

  } catch (err) {
    return err;
  }
}

export default {
  getValuedBalance,
  getUserCurrencies,
  getChartData,
}