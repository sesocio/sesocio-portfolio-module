import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

const state = {
  valuedBalance: 0,
  portfolioType: "crypto",
  userCurrencies: [],
  selectedCurrency: null,
  chartData: [],
  loading: false,
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
};