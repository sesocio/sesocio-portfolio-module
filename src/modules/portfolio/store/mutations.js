const SET_VALUED_BALANCE = (state, payload) => {
  state.valuedBalance = payload;
};
const SET_PORTFOLIO_TYPE = (state, payload) => {
  state.portfolioType = payload;
};
const SET_USER_CURRENCIES = (state, payload) => {
  state.userCurrencies = payload;
};
const SET_SELECTED_CURRENCY = (state, payload) => {
  state.selectedCurrency = payload;
};
const SET_CHART_DATA = (state, payload) => {
  state.chartData = payload;
};
const SET_LOADING = (state, payload) => {
  state.loading = payload;
};
const CLEAR_DATA = (state) => {
  state.valuedBalance = 0;
  state.chartData = [];
};

export default {
  SET_VALUED_BALANCE,
  SET_PORTFOLIO_TYPE,
  SET_USER_CURRENCIES,
  SET_SELECTED_CURRENCY,
  SET_CHART_DATA,
  SET_LOADING,
  CLEAR_DATA
};