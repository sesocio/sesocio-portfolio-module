// Routes
import routesPortfolio from './src/modules/portfolio/routes';

// Store
import storePortfolio from './src/modules/portfolio/store';

export {
	routesPortfolio as SesocioPortfolioRoute,
  storePortfolio as SesocioPortfolioStore,
};
