const portfolioType = state => state.portfolioType;
const selectedCurrency = state => state.selectedCurrency;

export default {
  portfolioType,
  selectedCurrency,
}